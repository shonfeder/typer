(* prelexer.ml --- First half of lexical analysis of Typer.

Copyright (C) 2011-2021  Free Software Foundation, Inc.

Author: Stefan Monnier <monnier@iro.umontreal.ca>
Keywords: languages, lisp, dependent types.

This file is part of Typer.

Typer is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any
later version.

Typer is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.  *)

open Util

let prelexer_error loc = Log.log_error ~section:"PRELEXER" ~loc

type pretoken =
  | Pretoken of location * string
  | Prestring of location * string
  | Preblock of location * pretoken list

module Pretoken = struct
  type t = pretoken

  (* Equality up to location, i.e. the location is not considered. *)
  let rec equal (l : t) (r : t) =
    match l, r with
    | Pretoken (_, l_name), Pretoken (_, r_name)
      -> String.equal l_name r_name
    | Prestring (_, l_text), Prestring (_, r_text)
      -> String.equal l_text r_text
    | Preblock (_, l_inner), Preblock (_, r_inner)
      -> Listx.equal equal l_inner r_inner
    | _ -> false
end

(*************** The Pre-Lexer phase *********************)

(* In order to allow things like "module Toto { open Titi; ... }" where
   lexical and syntatic declarations from Titi do impact the parsing of
   "...", the "module" macro has to receive an unparsed form of "...".

   So Sexps will keep the {...} blocks "unparsed" and macros that take such
   arguments will want to call the reader on them explicitly.  But in order
   to avoid traversing blocks as many times as they are nested, we perform
   the "block recognition" once and for all at the start via a "pre-lexer".
   This also handles strings and comments since they may contain braces
   (contrary to numbers, for example which are not handled here).  *)

(* FIXME: Add syntax for char constants (maybe 'c').  *)
(* FIXME: Handle multiline strings.  *)

let rec consume_until_newline (source : #Source.t) : unit =
  match source#next with
  | None | Some ('\n') -> ()
  | Some _ -> consume_until_newline source

(* Splits a Typer source into pretokens, stopping when it is completely
   consumed. *)
let prelex (source : #Source.t) : pretoken list =
  let rec loop
            (ctx : (Source.Point.t * pretoken list) list)
            (acc : pretoken list)
          : pretoken list =

    match source#peek with
    | None
      -> (match ctx with
         | [] -> List.rev acc
         | ((brace_point, _) :: _)
           -> let location = source#make_location brace_point in
              prelexer_error location "Unmatched opening brace";
              List.rev acc)

    | Some (c) when c <= ' '
      -> source#advance;
         loop ctx acc

    (* A comment.  *)
    | Some ('%')
      -> consume_until_newline source;
         loop ctx acc

    (* A string.  *)
    | Some ('"')
      -> let quote_point = source#point in
         source#advance;
         let rec prestring chars =
           match source#peek with
           | None | Some ('\n')
             -> source#advance;
                let location = source#make_location quote_point in
                prelexer_error location "Unterminated string";
                loop ctx (Prestring (location, "") :: acc)

           | Some ('"')
             -> source#advance;
                let location = source#make_location quote_point in
                let pretoken = Prestring (location, string_implode (List.rev chars)) in
                loop ctx (pretoken :: acc)

           | Some ('\\')
             -> let escape_point = source#point in
                source#advance;
                (match source#next with
                 | None | Some ('\n')
                   -> let location = source#make_location escape_point in
                      prelexer_error location "Unterminated escape sequence";
                      loop ctx (Prestring (location, "") :: acc)
                 | Some ('t') -> prestring ('\t' :: chars)
                 | Some ('n') -> prestring ('\n' :: chars)
                 | Some ('r') -> prestring ('\r' :: chars)
                 | Some ('u')
                   -> let location = source#make_location escape_point in
                      prelexer_error location "Unimplemented unicode escape";
                      prestring chars
                 | Some (c) -> prestring (c :: chars))

           | Some (char)
             -> source#advance;
                prestring (char :: chars)
         in prestring []

    | Some ('{')
      -> let point = source#point in
         source#advance;
         loop ((point, acc) :: ctx) []

    | Some ('}')
      -> let closing_brace_point = source#point in
         source#advance;
         (match ctx with
          | ((opening_brace_point, sacc) :: ctx)
            -> let location = source#make_location opening_brace_point in
               let preblock = Preblock (location, List.rev acc) in
               loop ctx (preblock :: sacc)
          | _
            -> let location = source#make_location closing_brace_point in
               prelexer_error location "Unmatched closing brace";
               loop ctx acc)

    (* A pretoken.  *)
    | Some _
      -> let start_point = source#point in
         source#advance;
         let rec pretok () =
           match source#peek with
           | None | Some ('%' | '"' | '{' | '}')
             -> let text, location = source#slice start_point in
                loop ctx (Pretoken (location, text) :: acc)

           | Some (c) when c <= ' '
             -> let text, location = source#slice start_point in
                source#advance;
                loop ctx (Pretoken (location, text) :: acc)

           | Some ('\\')
             -> let escape_point = source#point in
                source#advance;
                (match source#next with
                 | Some _ -> pretok ()
                 | None
                   -> let error_location = source#make_location escape_point in
                      source#advance;
                      let text, location = source#slice start_point in
                      prelexer_error
                        error_location
                        "Unterminated escape sequence in: %s"
                        text;
                      loop ctx (Pretoken (location, text) :: acc))

           | Some _
             -> source#advance;
                pretok ()
         in pretok ()
  in
  loop [] []

let pretoken_name : pretoken -> string = function
  | Pretoken _ -> "Pretoken"
  | Prestring _ -> "Prestring"
  | Preblock  _ -> "Preblock"

let rec pretoken_string' (output : string -> unit) : pretoken -> unit = function
  | Preblock (_, []) ->
     output "{}"
  | Preblock (_, head :: tail) ->
     output "{";
     pretoken_string' output head;
     List.iter (fun pt -> output " "; pretoken_string' output pt) tail;
     output "}"
  | Pretoken (_, text) ->
     output text
  | Prestring (_, text) ->
     output {|"|};
     output text;
     output {|"|}

let pretoken_string (pretoken : pretoken) : string =
  let buffer = Buffer.create 32 in
  pretoken_string' (Buffer.add_string buffer) pretoken;
  Buffer.contents buffer

let pretokens_string (pretokens : pretoken list) : string =
  let buffer = Buffer.create 32 in
  List.iter (pretoken_string' (Buffer.add_string buffer)) pretokens;
  Buffer.contents buffer

let pretokens_print : pretoken list -> unit =
  List.iter (pretoken_string' print_string)

(* Prelexer comparison, ignoring source-line-number info, used for tests.  *)
let rec pretokens_equal p1 p2 = match p1, p2 with
  | Pretoken (_, s1), Pretoken (_, s2) -> s1 = s2
  | Prestring (_, s1), Prestring (_, s2) -> s1 = s2
  | Preblock (_, ps1), Preblock (_, ps2) ->
     pretokens_eq_list ps1 ps2
  | _ -> false
and pretokens_eq_list ps1 ps2 = match ps1, ps2 with
  | [], [] -> true
  | (p1 :: ps1), (p2 :: ps2) ->
     pretokens_equal p1 p2 && pretokens_eq_list ps1 ps2
  | _ -> false
