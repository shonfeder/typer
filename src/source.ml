(* Copyright (C) 2021  Free Software Foundation, Inc.
 *
 * Author: Simon Génier <simon.genier@umontreal.ca>
 * Keywords: languages, lisp, dependent types.
 *
 * This file is part of Typer.
 *
 * Typer is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>. *)

module Point = struct
  (* offset * line * column *)
  type t = int * int * int

  let equal (l, _, _ : t) (r, _, _ : t) : bool =
    (* The line and column are metadata, only the offset is necessary for
       determining equality. *)
    Int.equal l r
end

module Location = struct
  type t =
    {
      file : string;
      start_line : int;
      start_column : int;
      end_line : int;
      end_column : int;
    }

  let dummy =
    {
      file = "";
      start_line = 0;
      start_column = 0;
      end_line = 0;
      end_column = 0;
    }

  (* Creates a zero-width location around the given point. *)
  let of_point (file : string) (_, line, column : Point.t) : t =
    {
      file;
      start_line = line;
      start_column = column;
      end_line = line;
      end_column = column;
    }

  let to_string
        ({file; start_line; start_column; end_line; end_column} : t)
      : string =

    if Int.equal start_line end_line && Int.equal start_column end_column
    then Printf.sprintf "%s:%d:%d" file start_line start_column
    else
      Printf.sprintf
        "%s:%d:%d-%d:%d" file start_line start_column end_line end_column

  let equal (l : t) (r : t) =
    let
      {
        file = l_file;
        start_line = l_start_line;
        start_column = l_start_column;
        end_line = l_end_line;
        end_column = l_end_column;
      } = l
    in
    let
      {
        file = r_file;
        start_line = r_start_line;
        start_column = r_start_column;
        end_line = r_end_line;
        end_column = r_end_column;
      } = r
    in

    String.equal l_file r_file
    && Int.equal l_start_line r_start_line
    && Int.equal l_start_column r_start_column
    && Int.equal l_end_line r_end_line
    && Int.equal l_end_column r_end_column
end

(* Traditionally, line numbers start at 1… *)
let first_line_of_file = 1
(* … and so do column numbers :-( *)
let first_column_of_line = 1

(* A source object is text paired with a cursor. The text can be lazily loaded
   as it is accessed byte by byte, but it must be retained for future reference
   by error messages. *)
class virtual t (base_line : int) (base_column : int) (file : string) =
object (self)
  val mutable line = base_line
  val mutable column = base_column

  (* A path if the text comes from a file, otherwise a meaningful identifier. *)
  method file : string = file

  (* Return the byte at the cursor, or None if the cursor is at the end of
     the text. *)
  method virtual peek : char option

  (* The current point of the cursor. *)
  method point : Point.t =
    (self#offset, line, column)

  (* The current offset of the cursor in the file, in bytes. *)
  method virtual private offset : int

  (* Makes a location starting at the given point and ending at the current
     cursor position. *)
  method make_location (_, start_line, start_column : Point.t) : Location.t =
    {file; start_line; start_column; end_line = line; end_column = column}

  (* Slices the text, from (and including) a starting point and to (and
     excluding) the curent cursor offset.

     Note that the source is required only to buffer the last line read and may
     raise an Invalid_argument if the slice extends before the start of the
     line. *)
  method slice (offset, _, _ as point : Point.t) : string * Location.t =
    (self#slice_impl offset, self#make_location point)

  method virtual private slice_impl : int -> string

  (* Moves the cursor forward one byte *)
  method advance : unit =
    let is_utf8_head c = Char.code c < 128 || Char.code c >= 192 in
    let c = self#peek in
    self#advance_impl;
    match c with
    | Some '\n'
      -> line <- line + 1;
         column <- 0;
    | Some c when is_utf8_head c
      -> column <- column + 1
    | _ -> ()

  method private virtual advance_impl : unit

  (* Returns the char at the cursor, then advances it forward. *)
  method next : char option =
    let c = self#peek in
    self#advance;
    c
end

let read_buffer_length = 4096

class source_file file_name = object (self)
  inherit t first_line_of_file first_column_of_line file_name

  val in_channel = open_in file_name
  val mutable end_of_line = false
  val mutable source_line = ""
  val mutable line_offset = 0
  val mutable offset = 0

  method private peek_unchecked =
    if offset < String.length source_line
    then source_line.[offset]
    else '\n'

  method peek =
    if offset <= String.length source_line
    then Some self#peek_unchecked
    else
      try
        line_offset <- line_offset + 1 + String.length source_line;
        source_line <- input_line in_channel;
        offset <- 0;
        Some self#peek_unchecked
      with
      | End_of_file -> None

  method private advance_impl =
    offset <- offset + 1

  method private offset = line_offset + offset

  method private slice_impl start_offset =
    let relative_start_offset = start_offset - line_offset in
    String.sub source_line relative_start_offset (offset - relative_start_offset)
end

class source_string
        ?(file : string = "<string>")
        ?(line : int = first_line_of_file)
        ?(column : int = first_column_of_line)
        source
      =
object
  inherit t line column file

  val mutable offset = 0

  method peek =
    if offset < String.length source
    then Some (source.[offset])
    else None

  method private advance_impl =
    offset <- offset + 1

  method private offset = offset

  method private slice_impl start_offset =
    String.sub source start_offset (offset - start_offset)
end
