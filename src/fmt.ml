(*
 *      Typer Compiler
 *
 * ---------------------------------------------------------------------------
 *
 *      Copyright (C) 2011-2020  Free Software Foundation, Inc.
 *
 *   Author: Pierre Delaunay <pierre.delaunay@hec.ca>
 *   Keywords: languages, lisp, dependent types.
 *
 *   This file is part of Typer.
 *
 *   Typer is free software; you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any
 *   later version.
 *
 *   Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *   more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ---------------------------------------------------------------------------
 *
 *      Description:
 *          Declare new print function which align printed values.
 *
 * ---------------------------------------------------------------------------*)

(* print n char 'c' *)
let make_line c n = String.make n c

(* Table Printing helper *)
let make_title title =
    let title_n = String.length title in
    let p = title_n mod 2 in
    let sep_n = (80 - title_n - 4) / 2 in
    let lsep = (make_line '=' sep_n) in
    let rsep = (make_line '=' (sep_n + p)) in
        ("    " ^ lsep ^ title ^ rsep ^ "\n")

let make_rheader (head: ((char * int) option * string) list) =
  print_string "    | ";
  let print_header (o, name) =
    (match o with
     | Some ('r', size) -> Printf.printf "%*s" size name
     | Some ('l', size) -> Printf.printf "%-*s" size name
     | _ -> print_string name);
    print_string " | "
  in
  List.iter print_header head;
  print_string "\n"

let make_sep c = "    " ^ (make_line c 76) ^ "\n"


(* used to help visualize the call trace *)
let print_ct_tree i =
    let rec loop j =
        if j = i then () else
        match j with
            | _ when (j mod 2) = 0 -> print_char '|'; loop (j + 1)
            | _ -> print_char ':'; loop (j + 1) in
    loop 0

(* Colors *)
let red     = "\x1b[31m"
let green   = "\x1b[32m"
let yellow  = "\x1b[33m"
let magenta = "\x1b[35m"
let cyan    = "\x1b[36m"
let reset   = "\x1b[0m"

let color_string color str =
  color ^ str ^ reset
