(*
 *      Typer Compiler
 *
 * ---------------------------------------------------------------------------
 *
 *      Copyright (C) 2011-2021  Free Software Foundation, Inc.
 *
 *   Author: Pierre Delaunay <pierre.delaunay@hec.ca>
 *   Keywords: languages, lisp, dependent types.
 *
 *   This file is part of Typer.
 *
 *   Typer is free software; you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any
 *   later version.
 *
 *   Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *   more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ---------------------------------------------------------------------------
 *
 *      Description:
 *          Specifies recursive data structure for DeBruijn indexing
 *          methods starting with '_' are considered private and should not
 *          elsewhere in the project
 *
 * ---------------------------------------------------------------------------*)

module Str = Str

open Util


open Lexp

module M = Myers
open Fmt

module S = Subst

let fatal ?print_action ?loc fmt =
  Log.log_fatal ~section:"DEBRUIJN" ?print_action ?loc fmt

(* Handling scoping/bindings is always tricky.  So it's always important
 * to keep in mind for *every* expression which is its context.
 *
 * In particular, this holds true as well for those expressions that appear
 * in the context.  Traditionally for dependently typed languages we expect
 * the context's rules to say something like:
 *
 *      ⊢ Γ    Γ ⊢ τ:Type
 *      —————————————————
 *          ⊢ Γ,x:τ
 *
 * Which means that we expect (τ) expressions in the context to be typed
 * within the *rest* of that context.
 *
 * This also means that when we look up a binding in the context, we need to
 * adjust the result, since we need to use it in the context where we looked
 * it up, which is different from the context where it was defined.
 *
 * More concretely, this means that lookup(Γ, i) should return an expression
 * where debruijn indices have been shifted by "i".
 *
 * This is nice for "normal bindings", but there's a complication in the
 * case of recursive definitions.  Typically, this is handled by using
 * something like a μx.e construct, which works OK for the theory but tends
 * to become rather inconvenient in practice for mutually recursive
 * definitions.  So instead, we annotate the recursive binding with
 * a "recursion_offset" (of type `db_offset`) to say that rather than being
 * defined in "the rest of the context", they're defined in a slightly larger
 * context that includes "younger" bindings.
 *)


(*  Type definitions
 * ---------------------------------- *)

let dloc   = dummy_location
let type_level_sort = mkSort (dloc, StypeLevel)
let sort_omega = mkSort (dloc, StypeOmega)
let type_level = mkBuiltin ((dloc, "TypeLevel"), type_level_sort)
let level0 = mkSortLevel SLz
let level1  = mkSortLevel (mkSLsucc level0)
let level2  = mkSortLevel (mkSLsucc level1)
let type0  = mkSort (dloc, Stype level0)
let type1   = mkSort (dloc, Stype level1)
let type2   = mkSort (dloc, Stype level2)
let type_int = mkBuiltin ((dloc, "Int"), type0)
let type_integer = mkBuiltin ((dloc, "Integer"), type0)
let type_float = mkBuiltin ((dloc, "Float"), type0)
let type_string = mkBuiltin ((dloc, "String"), type0)
let type_elabctx = mkBuiltin ((dloc, "Elab_Context"), type0)
let type_eq_type =
  let lv = (dloc, Some "l") in
  let tv = (dloc, Some "t") in
  mkArrow (Aerasable, lv,
           type_level, dloc,
           mkArrow (Aerasable, tv,
                    mkSort (dloc, Stype (mkVar (lv, 0))), dloc,
                    mkArrow (Anormal, (dloc, None),
                             mkVar (tv, 0), dloc,
                             mkArrow (Anormal, (dloc, None),
                                      mkVar (tv, 1), dloc,
                                      mkSort (dloc, Stype (mkVar (lv, 3)))))))
let type_eq = mkBuiltin ((dloc, "Eq"), type_eq_type)
let eq_refl =
  let lv = (dloc, Some "l") in
  let tv = (dloc, Some "t") in
  let xv = (dloc, Some "x") in
  mkBuiltin ((dloc, "Eq.refl"),
             mkArrow (Aerasable, lv,
                      type_level, dloc,
                      mkArrow (Aerasable, tv,
                               mkSort (dloc, Stype (mkVar (lv, 0))), dloc,
                               mkArrow (Aerasable, xv,
                                        mkVar (tv, 0), dloc,
                                        mkCall (type_eq,
                                                [Aerasable, mkVar (lv, 2);
                                                 Aerasable, mkVar (tv, 1);
                                                 Anormal, mkVar (xv, 0);
                                                 Anormal, mkVar (xv, 0)])))))


(* easier to debug with type annotations *)
type env_elem = (vname * varbind * ltype)
type lexp_context = env_elem M.myers

type db_ridx = int (* DeBruijn reverse index (i.e. counting from the root).  *)

(*  Map variable name to its distance in the context *)
type senv_length = int  (* it is not the map true length *)
type senv_type = senv_length * (db_ridx SMap.t)

type lctx_length = db_ridx

type meta_scope
  = scope_level       (* Integer identifying a level.  *)
    * lctx_length     (* Length of ctx when the scope is added.  *)
    * (meta_id SMap.t ref) (* Metavars already known in this scope.  *)

(* This is the *elaboration context* (i.e. a context that holds
 * a lexp context plus some side info.  *)
type elab_context
  = Grammar.grammar * senv_type * lexp_context * meta_scope

let get_size (ctx : elab_context)
  = let (_, (n, _), lctx, _) = ctx in
    assert (n = M.length lctx); n

let ectx_to_grm (ectx : elab_context) : Grammar.grammar =
  let (grm,_, _, _) = ectx in grm

  (* Extract the lexp context from the context used during elaboration.  *)
let ectx_to_lctx (ectx : elab_context) : lexp_context =
  let (_,_, lctx, _) = ectx in lctx

let ectx_to_scope_level ((_, _, _, (sl, _, _)) : elab_context) : scope_level
  = sl

let ectx_local_scope_size ((_, (_n, _), _, (_, slen, _)) as ectx) : int
  = get_size ectx - slen

(*  Public methods: DO USE
 * ---------------------------------- *)

let empty_senv = (0, SMap.empty)
let empty_lctx = M.nil

let empty_elab_context : elab_context
  = (Grammar.default_grammar, empty_senv, empty_lctx,
     (0, 0, ref SMap.empty))

(* senv_lookup caller were using Not_found exception *)
exception Senv_Lookup_Fail of (string list)
let senv_lookup_fail relateds = raise (Senv_Lookup_Fail relateds)

(* Return its current DeBruijn index.  *)
let senv_lookup (name: string) (ctx: elab_context): int =
  let (_, (n, map), _, _) = ctx in
  try n - (SMap.find name map) - 1
  with Not_found
       -> let get_related_names (_n : db_ridx) name map =
           let r = Str.regexp (".*"^name^".*") in
           let search r = SMap.fold (fun name _ names
                                     -> if (Str.string_match r name 0) then
                                         name::names
                                       else
                                         names)
                                    map [] in
           if ((String.sub name 0 1) = "_" ||
                 (String.sub name ((String.length name) - 1) 1) = "_") &&
                ((String.length name) > 1) then
             search r
           else [] in

         senv_lookup_fail (get_related_names n name map)

let lexp_ctx_cons (ctx : lexp_context) d v t =
  assert (let offset = match v with | LetDef (o, _) -> o | _ -> 0 in
          offset >= 0
          && (ctx = M.nil
             || match M.car ctx with
               | (_, LetDef (previous_offset, _), _)
                 -> previous_offset >= 0 (* General constraint.  *)
                   (* Either `ctx` is self-standing (doesn't depend on us),
                    * or it depends on us (and maybe other bindings to come), in
                    * which case we have to depend on the exact same bindings.  *)
                   && (previous_offset <= 1
                      || previous_offset = 1 + offset)
               | _ -> true));
  M.cons (d, v, t) ctx

let lctx_extend (ctx : lexp_context) (def: vname) (v: varbind) (t: lexp) =
  lexp_ctx_cons ctx def v t

let env_extend_rec (ctx: elab_context) (def: vname) (v: varbind) (t: lexp) =
  let (_loc, oname) = def in
  let (grm, (n, map), env, sl) = ctx in
  let nmap = match oname with None -> map | Some name -> SMap.add name n map in
  (grm, (n + 1, nmap),
   lexp_ctx_cons env def v t,
   sl)

let ectx_extend (ctx: elab_context) (def: vname) (v: varbind) (t: lexp) = env_extend_rec ctx def v t

let lctx_extend_rec (ctx : lexp_context) (defs: (vname * lexp * ltype) list) =
  let (ctx, _) =
    List.fold_left
      (fun (ctx, recursion_offset) (def, e, t) ->
        lexp_ctx_cons ctx def (LetDef (recursion_offset, e)) t,
        recursion_offset - 1)
      (ctx, List.length defs) defs in
  ctx

let ectx_extend_rec (ctx: elab_context) (defs: (vname * lexp * ltype) list) =
  let (grm, (n, senv), lctx, sl) = ctx in
  let senv', _ = List.fold_left
                   (fun (senv, i) ((_, oname), _, _) ->
                     (match oname with None -> senv
                                     | Some name -> SMap.add name i senv),
                     i + 1)
                   (senv, n) defs in
  (grm, (n + List.length defs, senv'), lctx_extend_rec lctx defs, sl)

let ectx_new_scope (ectx : elab_context) : elab_context =
  let (grm, senv, lctx, (scope, _, rmmap)) = ectx in
  (grm, senv, lctx, (scope + 1, Myers.length lctx, ref (!rmmap)))

let ectx_get_scope (ectx : elab_context) : meta_scope =
  let (_, _, _, sl) = ectx in sl

let ectx_get_grammar (ectx : elab_context) : Grammar.grammar =
  let (grm, _, _, _) = ectx in grm

let env_lookup_by_index index (ctx: lexp_context): env_elem =
  Myers.nth index ctx

let print_lexp_ctx_n (ctx : lexp_context) (ranges : (int * int) list) =
  print_string (make_title " LEXP CONTEXT ");

  make_rheader
    [(Some ('l',  7), "INDEX");
     (Some ('l',  4), "OFF");
     (Some ('l', 10), "NAME");
     (Some ('l', 42), "VALUE : TYPE")];

  print_string (make_sep '-');

  let prefix = "    |         |      |            | " in

  let print i =
    try
      let r, name, lexp, ty =
        match env_lookup_by_index i ctx with
        | ((_, name), LetDef (r, exp), ty) -> r, name, Some exp, ty
        | ((_, name), _, ty) -> 0, name, None, ty
      in
      let name' = maybename name in
      let short_name =
        if String.length name' > 10
        then
          String.sub name' 0 9 ^ "…"
        else name'
      in
      Printf.printf "    | %-7d | %-4d | %-10s | " i r short_name;
      (match lexp with
       | None -> print_string "<var>"
       | Some lexp
         -> (let str = lexp_str (!debug_ppctx) lexp in
             let strs =
               match String.split_on_char '\n' str with
               | hd :: tl -> print_string hd; tl
               | [] -> []
             in
             List.iter
               (fun elem ->
                 print_newline ();
                 print_string prefix;
                 print_string elem)
               strs));
      print_string " : ";
      lexp_print ty;
      print_newline ()
    with
    | Not_found
      -> print_endline "    | %-7d | Not_found  |"
  in

  let rec print_range lower i =
    let i' = i - 1 in
    if i' >= lower
    then
      (print i';
       print_range lower i')
  in

  let rec print_ranges = function
    | [] -> ()
    | (lower, upper) :: ranges'
      -> (print_range lower upper;
          print_ranges ranges')
  in

  print_ranges ranges;
  print_string (make_sep '=')

(* Only print user defined variables *)
let print_lexp_ctx (lctx : lexp_context) : unit =
  print_lexp_ctx_n lctx [(0, M.length lctx - !builtin_size)]

(* Dump the whole context *)
let dump_lexp_ctx (lctx : lexp_context) : unit =
  print_lexp_ctx_n lctx [(0, M.length lctx)]

let summarize_lctx (lctx : lexp_context) (at : int) : unit =
  let ranges =
    if at < 7
    then [(0, min (max (at + 2) 5) (M.length lctx))]
    else [(at - 2, min (at + 2) (M.length lctx)); (0, 5)]
  in
  print_lexp_ctx_n lctx ranges;
  print_endline "This context was trucated. Pass the option -Vfull-lctx to view it in full."

let log_full_lctx = ref false

(* generic lookup *)
let lctx_lookup (ctx : lexp_context) (v: vref): env_elem  =
  let ((loc, oename), dbi) = v in
  try
    let ret = Myers.nth dbi ctx in
    let _ = match (ret, oename) with
      | (((_, Some name), _, _), Some ename)
        -> (* Check if names match *)
           if not (ename = name) then
             let print_action =
               if !log_full_lctx
               then fun () -> (print_lexp_ctx ctx; print_newline ())
               else fun () -> summarize_lctx ctx dbi
             in
             fatal
               ~loc ~print_action
               ({|DeBruijn index %d refers to wrong name.  |}
                ^^ {|Expected: "%s" got "%s"|})
               dbi ename name
      | _ -> () in

    ret
  with
  | Not_found
    -> fatal
         ~loc "DeBruijn index %d of `%s` out of bounds" dbi (maybename oename)

let lctx_lookup_type (ctx : lexp_context) (vref : vref) : lexp =
  let (_, i) = vref in
  let (_, _, t) = lctx_lookup ctx vref in
  mkSusp t (S.shift (i + 1))

let lctx_lookup_value (ctx : lexp_context) (vref : vref) : lexp option =
  let (_, i) = vref in
  match lctx_lookup ctx vref with
  | (_, LetDef (o, v), _) -> Some (push_susp v (S.shift (i + 1 - o)))
  | _ -> None

let env_lookup_type ctx (v : vref): lexp =
  lctx_lookup_type (ectx_to_lctx ctx) v

    (* mkSusp ltp (S.shift (idx + 1)) *)

let env_lookup_expr ctx (v : vref): lexp option =
  lctx_lookup_value (ectx_to_lctx ctx) v

type lct_view =
  | CVempty
  | CVlet of vname * varbind * ltype * lexp_context
  | CVfix of (vname * lexp * ltype) list * lexp_context

let lctx_view lctx =
  match lctx with
  | Myers.Mnil -> CVempty
  | Myers.Mcons ((loname, LetDef (1, def), t), lctx, _, _)
    -> let rec loop i lctx defs =
        match lctx with
        | Myers.Mcons ((loname, LetDef (o, def), t), lctx, _, _)
             when o = i
          -> loop (i + 1) lctx ((loname, def, t) :: defs)
        | _ -> CVfix (defs, lctx) in
      loop 2 lctx [(loname, def, t)]
  | Myers.Mcons ((_, LetDef (o, _def), _), _, _, _) when o > 1
    -> fatal "Unexpected lexp_context shape!"
  | Myers.Mcons ((loname, odef, t), lctx, _, _)
    -> CVlet (loname, odef, t, lctx)

(**          Sets of DeBruijn indices          **)

type set = db_offset * unit IMap.t

let set_empty = (0, IMap.empty)

let set_mem i (o, m) = IMap.mem (i - o) m

let set_set i (o, m) = (o, IMap.add (i - o) () m)
let set_reset i (o, m) = (o, IMap.remove (i - o) m)

let set_singleton i = (0, IMap.singleton i ())

(* Adjust a set for use in a deeper scope with `o` additional bindings.  *)
let set_sink o (o', m) = (o + o', m)

(* Adjust a set for use in a higher scope with `o` fewer bindings.  *)
let set_hoist o (o', m) =
  let newo = o' - o in
  let (_, _, newm) = IMap.split (-1 - newo) m
  in (newo, newm)

let set_union (o1, m1) (o2, m2) : set =
  if o1 = o2 then
    (o1, IMap.merge (fun _k _ _ -> Some ()) m1 m2)
  else
    let o = o2 - o1 in
    (o1, IMap.fold (fun i2 () m1
                    -> IMap.add (i2 + o) () m1)
                   m2 m1)
