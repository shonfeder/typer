{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  name = "typer";
  buildInputs =
    with pkgs.ocamlPackages; [
      pkgs.gnumake ocaml dune_2 findlib utop # tooling
      zarith # ocaml libraries
      merlin # for emacs
    ];
}
