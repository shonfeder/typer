(* Copyright (C) 2021  Free Software Foundation, Inc.
 *
 * Author: Simon Génier <simon.genier@umontreal.ca>
 * Keywords: languages, lisp, dependent types.
 *
 * This file is part of Typer.
 *
 * Typer is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>. *)

(** This file is the the entry point to the typer REPL. *)

open Typerlib

open Debruijn

open Printf

let arg_batch = ref false

let add_input_file, list_input_files =
  let files = ref [] in
  (fun file -> files := file :: !files), (fun () -> List.rev !files)

let arg_defs =
  [("--verbosity",
    Arg.String Log.set_typer_log_level_str,
    "Set the logging level");

   ("-v", Arg.Unit Log.increment_log_level, "Increment verbosity");

   ("-Vfull-lctx",
    Arg.Set Debruijn.log_full_lctx,
    "Print the full lexp context on error");

   ("-Vmacro-expansion",
    Arg.Set Elab.macro_tracing_enabled,
    "Trace macro expansion");
  ]

let parse_args argv usage =
  try Arg.parse_argv argv arg_defs add_input_file usage with
  | Arg.Help (message) ->
     print_string message;
     exit 0

let compile_main argv =
  let usage = Sys.executable_name ^ " compile [options] <file> …" in
  parse_args argv usage;

  let ectx = Elab.default_ectx in
  let backend = new Gambit.gambit_compiler (ectx_to_lctx ectx) stdout in

  let _ =
    List.fold_left (Elab.process_file backend) ectx (list_input_files ())
  in
  Log.print_log ()

let repl_main argv =
  let usage = Sys.executable_name ^ " repl [options] [<file>] …" in
  parse_args argv usage;

  print_string (Fmt.make_title " TYPER REPL ");
  print_endline "      Typer 0.0.0 - Interpreter - (c) 2016-2021";
  print_newline ();
  print_endline "      %quit         (%q) : leave REPL";
  print_endline "      %help         (%h) : print help";
  print_string (Fmt.make_sep '-');
  flush stdout;

  let ectx = Elab.default_ectx in
  let backend = new Eval.ast_interpreter (ectx_to_lctx ectx) in

  let load_file (ectx,  i) file_name =
    printf "  In[% 2d] >> %%readfile %s\n" i file_name;
    Elab.process_file backend ectx file_name, i + 1
  in
  let ectx, i = List.fold_left load_file (ectx, 0) (list_input_files ()) in
  Log.print_log ();
  REPL.repl i backend ectx

let run_main argv =
  let usage = Sys.executable_name ^ " run [options] <file> …" in
  parse_args argv usage;

  let ectx = Elab.default_ectx in
  let backend = new Eval.ast_interpreter (ectx_to_lctx ectx) in

  let _ =
    List.fold_left (Elab.process_file backend) ectx (list_input_files ())
  in
  Log.print_log ()

let main () =
  let command, argv =
    if Array.length Sys.argv <= 1
    then "repl", [||]
    else
      let command = Sys.argv.(1) in
      if command.[0] = '-' || String.contains command '.'
      then "repl", Sys.argv
      else
        (command,
         Array.append
           [|Sys.argv.(0)|]
           (Array.sub Sys.argv 2 (Array.length Sys.argv - 2)))
  in

  Log.handle_error
    ~on_error:(fun () -> exit 1)
    (fun () ->
      match command with
      | "compile" -> compile_main argv
      | "repl" -> repl_main argv
      | "run" -> run_main argv
      | _
        -> eprintf {|unknown command "%s"|} command;
           exit 1)

let () = main ()
