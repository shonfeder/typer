(* Copyright (C) 2021  Free Software Foundation, Inc.
 *
 * Author: Simon Génier <simon.genier@umontreal.ca>
 * Keywords: languages, lisp, dependent types.
 *
 * This file is part of Typer.
 *
 * Typer is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>. *)

open Typerlib

open Utest_lib

open Prelexer
open Sexp
open Source

let test_lex name pretokens expected =
  (* Unlike the version in the Sexp module, this version of equality considers
     locations. *)
  let rec sexp_equal actual expected =
    match actual, expected with
    | Sexp.Block (actual_location, actual_pretokens),
      Sexp.Block (expected_location, expected_pretokens)
      -> Location.equal actual_location expected_location
         && Listx.equal Pretoken.equal actual_pretokens expected_pretokens
    | Sexp.Symbol (actual_location, actual_name),
      Sexp.Symbol (expected_location, expected_name)
      -> Location.equal actual_location expected_location
         && String.equal actual_name expected_name
    | Sexp.Node (actual_head, actual_tail),
      Sexp.Node (expected_head, expected_tail)
      -> sexp_equal actual_head expected_head
         && Listx.equal sexp_equal actual_tail expected_tail
    | _, _ -> false
  in
  let lex () =
    let actual = Lexer.lex Grammar.default_stt pretokens in
    if Listx.equal sexp_equal actual expected
    then success
    else
      ((* Only print locations if the Sexp are otherwise identical so its easier
          to spot the problem. *)
       let print_locations =
         Listx.equal Sexp.sexp_equal actual expected
       in
       let print_token token =
         Printf.printf "%s\n" (sexp_string ~print_locations token)
       in
       Printf.printf "%sExpected:%s\n" Fmt.red Fmt.reset;
       List.iter print_token expected;
       Printf.printf "%sActual:%s\n" Fmt.red Fmt.reset;
       List.iter print_token actual;
       failure)
  in
  add_test "LEXER" name lex

let l : Source.Location.t =
  {
    file = "test.typer";
    start_line = 1;
    start_column = 1;
    end_line = 1;
    end_column = 1;
  }

let () =
  test_lex
    "Inner operator inside a presymbol"
    [Pretoken (l, "a.b")]
    [Node (Symbol ({l with start_column = 2; end_column = 3}, "__.__"),
           [Symbol ({l with start_column = 1; end_column = 2}, "a");
            Symbol ({l with start_column = 3; end_column = 4}, "b")])]

let () =
  test_lex
    "Inner operators at the beginning of a presymbol"
    [Pretoken (l, ".b")]
    [Node (Symbol ({l with start_column = 1; end_column = 2}, "__.__"),
           [epsilon {l with start_column = 1; end_column = 1};
            Symbol ({l with start_column = 2; end_column = 3}, "b")])]

let () =
  test_lex
    "Inner operators at the end of a presymbol"
    [Pretoken (l, "a.")]
    [Node (Symbol ({l with start_column = 2; end_column = 3}, "__.__"),
           [Symbol ({l with start_column = 1; end_column = 2}, "a");
            epsilon {l with start_column = 3; end_column = 3}])]

let () =
  test_lex
    "An inner operator by itself is a simple symbol"
    [Pretoken (l, ".")]
    [Symbol ({l with start_column = 1; end_column = 2}, ".")]

let () = run_all ()
