%%% HoTT -- Homotopy Type-Theory

%%      Copyright (C) 2020  Free Software Foundation, Inc.
%%
%% Author: Stefan Monnier <monnier@iro.umontreal.ca>
%%
%% This file is part of Typer.
%%
%% Typer is free software; you can redistribute it and/or modify it under the
%% terms of the GNU General Public License as published by the Free Software
%% Foundation, either version 3 of the License, or (at your option) any
%% later version.
%%
%% Typer is distributed in the hope that it will be useful, but WITHOUT ANY
%% WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
%% FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
%% more details.
%%
%% You should have received a copy of the GNU General Public License along
%% with this program.  If not, see <http://www.gnu.org/licenses/>.

%%% Commentary:

%% Various definitions inspired from the HoTT book.

%%% Code:

%%%% Paths

%% Cubical interval type `I`
%% Path type:
%%
%%     I : Type0?
%%     i₀ : I;
%%     i₁ : I;
%%
%% Note: while one cannot eliminate (no `if`) on `I`, one can use
%% `∨`, `∧`, and `¬` on values of this type, and maybe one could do
%% `type I | i₀ | i₁` and allow elimination with the simple restriction
%% that you can only eliminate an `I` to another `I` (so one could define
%% `∨` by hand).  Better yet, maybe instead of a special `I` we could simply
%% use a normal `Bool`, since the `≡>` already prevents elimination?
%%
%%     Eq : (t : Type) ≡> t → t → Type;
%%     path : (p : I ≡> t) → Eq (p(_ := i₀)) (p(_ := i₁));
%%
%% We additionally need some kind of elimination on paths like:
%%
%%     Eq_call : Eq(_ := ?t) ?x ?y ≡> I ≡> ?t;
%%       Eq_call (_ : Eq x y) i₀ ↝ x
%%       Eq_call (_ : Eq x y) i₁ ↝ y
%%
%% Heterogenous equality could look like:
%%
%%     HEq (t₁ : Type) ≡> (t₂ : Type) ≡> t₁ → t₂ → Type;
%%     type HEq x₁ x₂
%%       | Hrefl (p : Eq t₁ t₂) (Eq (coe p x₁) x₂);
%%     type HEq (t : (i : I) ≡> Type)
%%                 (x₁ : t(i := i₀)) (x₂ : t(i := i₁))
%%       | Hrefl (p : Eq t₁ t₂) (Eq (coe p x₁) x₂);

%%%% Univalence

%% type Equiv_function (f : ?A -> ?B) (g : ?A -> ?B)
%%   | equiv_function ((x : ?) -> Eq (f x) (g x));
Equiv_function f (g : (x : ?A) -> ?B) = ((x : ?A) -> Eq (f x) (g x));
%% Equiv_function_axiom : Equiv_function ?f ?g -> Eq ?f ?g;

%% type HoTT_IsEquiv (f : ?A -> ?B)
%%   | hott_isequiv (Equiv_function (compose f ?g) identity)
%%                  (Equiv_function (compose ?h f) identity);

%% type Equiv_type (A : Type) (B : Type)
%%   | equiv_type (f : A -> B) (HoTT_Isequiv f);
%% univalence_axiom : Equiv_type A B -> Eq A B;

%% FIXME: Univalence is incompatible with Typer's `Eq_cast` because
%% it allows casting between, say, `Nat` and `BinNat` which is not
%% a no-op.  Cubical Agda supports it by making its "Eq elimination" into a
%% non-trivial operation whose `Eq` proof is very much non-erasable.
%%
%% It'd be great to support univalence without losing `Eq_cast`, but
%% it's not at all clear how:
%% - One way is to make it a non-axiom and implement it as a system that
%%   proves it directly on a case-by-case basis, as in the paper
%%   "Equivalence for Free!".
%% - Another is to make `Eq_cast` take a proof of `isSet T`,
%%   but that prevents use of `Eq_cast` between `Nat` and `α`
%%   since `isSet Type` is not true.
%%   It would also prevent use of `Eq_cast` on HIT.
%% - Ideally another would be to require `Eq_cast` to take an additional
%%   proof that the equality proof is equal to `eq_refl` or more generally:
%%
%%       Eq_cast: (eq: Eq ?x ?y) ≡> Eq_erasable eq ≡> ...
%%
%%   but that begs the question: how could we prevent "promoting"
%%   a non-erasable proof to an erasable one?
%% - Another is to distinguish univalent universes, as done in the paper
%%   "Extending Homotopy Type Theory with Strict Equality".
%%   E.g. Our sorts `Type ℓ` would be refined to `Type k ℓ` where `k`
%%   would be a boolean indicating if the universe admits univalence or not,
%%   and then the J (aka "cast") rule would be restricted so that a proof
%%   about equality between univalent types can only be used to coerce
%%   between univalent types.
%%   This is reminiscent of the confinement of Prop in Coq, so we could
%%   similarly restrict the univalent universes so they're always erased
%%   before runtime (so we don't need to have the non-nop form of "cast"
%%   at run-time).
%%   This can be thought of as adding a new equality to some universes,
%%   aka making a "quotient universe", which, like HIT, requires proving
%%   that every time we use types from this universe we obey those equalities.

%%%% Propositions, resizing, etc...

HoTT_isSet A = (x : A) -> (y : A) -> (p : Eq x y) -> (q : Eq x y) -> Eq p q;
%% `isProp` basically implies proof irrelevance.
%% So it also implies erasability.  Note that if we use the double-negation
%% encoding of classical `or` in type-theory, then it preserves `isProp`!
HoTT_isProp P = (x : P) -> (y : P) -> Eq x y;

%% Provable without axioms:
%%
%%     ¬¬¬A -> ¬A

Inverse_double_negation : ?A -> Not (Not ?A);
Inverse_double_negation a na = na a;

Weak_double_negation : Not (Not (Not ?A)) -> Not ?A;
Weak_double_negation nnna a = nnna (lambda na -> na a);

%% BEWARE: univalence_axiom incompatible with general LEM!
%% HoTT_LEM_axiom : HoTT_isProp A -> ¬¬A -> A;

%% "mere propositions" can be treated impredicatively!!
%% HoTT_propositional_resizing_axiom :
%%    Equiv_type { A : Type l | isProp A} { A : Type (l + 1) | isProp A}

%% Propositional truncation: ||A|| is equivalent to A but is a mere proposition.
%% One way to approximate could be:
Propositional_truncation A = (P : ?) ≡> HoTT_isProp P ≡> (A -> P) -> P;
propositional_truncation : ?A -> Propositional_truncation ?A;
propositional_truncation a f = f a;



%%% hott.typer ends here.
