%%% hurkens.typer --- Hurken's paradox

%%% Commentary:

%% @InProceedings{Hurkens95,
%%   author =       {Antonius Hurkens},
%%   title =        {A simplification of {G}irard's paradox},
%%   crossref =     {TLCA95},
%%   url =          {http://www.cs.cmu.edu/~kw/scans/hurkens95tlca.pdf},
%%   pages =        {266-278}
%% }

%%% Code:

* = Type_ (s z);
□ = Type_ (s (s z));

⊥ : Type;
⊥ = (p : Type) ≡> p;

¬ : (ℓ : TypeLevel) ≡> Type_ ℓ -> Type_ ℓ;
¬ t = t -> ⊥ ;

%% 𝓅 is the "powerset".
%% 𝓅 S = (S -> *);
𝓅 : (ℓ : TypeLevel) => Type_ (s ℓ) -> Type_ (s ℓ);
%% FIXME: `?` instead of `ℓ` below leads to a type error because it
%% infers a type `Type_ (s (ℓ ∪ ℓ))` instead of `Type_ (s ℓ)` :-(
𝓅 S = (S -> Type_ ℓ);
𝓅𝓅 S = 𝓅 (𝓅 S);

%%%% A "powerful" universe (U, σ, τ).
%% We need that ∀ C : 𝓅𝓅 U
%%
%%     σ(τ C) = {X | {y|τ(σ y) ∈ X} ∈ C}
%%
%% i.o.w
%%
%%     (σ ∘ τ) = (τ ∘ σ)**
%%
%% where * is ...

U : □;
U = (X : □) ≡> (𝓅𝓅 X -> X) -> 𝓅𝓅 X; % Uses the (▵,□,□) rule!

τ : 𝓅𝓅 U -> U;
τ t = lambda (X : □)
  ≡> lambda (f : (𝓅𝓅 X) -> X)
  -> lambda (p : 𝓅 X)
  -> t (lambda (y : U) -> p (f ((y (X := X) f))));

σ : U -> 𝓅𝓅 U;
σ (s : U) = (s (X := U) (lambda (t : 𝓅𝓅 U) -> τ t));

τσ y = τ (σ y);
%% στ x = σ (τ x);

Δ : 𝓅 U;
Δ = lambda (y : U) -> ¬((p : 𝓅 U) ≡> σ y p -> p (τσ y));

%% Ω = τ {X | X is inductive}
Ω : U;
Ω = τ (lambda (p : 𝓅 U) -> ((y : U) ≡> σ y p -> p y));

%%%% Proof that Ω is both well-founded and not

%% FIXME: Not sure if this means well-founded or the opposite!
well-founded = ((p : 𝓅 U) ≡> ((y : U) ≡> σ y p -> p y) -> p Ω);

boom1 : ¬ well-founded;
boom1 = lambda (v0 : (p : 𝓅 U) ≡> ((y : U) ≡> σ y p -> p y) -> p Ω)
  -> v0 (p := Δ)
        (lambda (y : U) ≡>
         lambda (v2 : σ y Δ) ->
         lambda (v3 : (p : 𝓅 U) ≡> σ y p -> p (τσ y))
         -> v3 (p := Δ)
               v2
               (lambda (p : 𝓅 U)
                ≡> v3 (p := lambda (y : U) -> p (τσ y))))
        (lambda (p : 𝓅 U) ≡> v0 (p := lambda (y : U) -> p (τσ y)));

boom2 : well-founded;
boom2 = lambda (p : 𝓅 U)
  ≡> lambda (v1 : (y : U) ≡> σ y p -> p y)
  -> v1 (y := Ω)
        %% FIXME: (lambda (x : U) ≡> v1 (y := τσ x))
        %% signals a spurious "DeBruijn index 1 refers to wrong name.
        %% Expected: `y` got `x`".
        (lambda (y : U) ≡> v1 (y := τσ y));

%% FIXME: Defining `boom` as below inf-loops, which I believe is a bug
%% (call it an accidental feature)!
%%boom : ⊥;
%%boom = boom1 boom2;

%% Summary:
%% U               = (X : □) ≡> (𝓅𝓅 X → X) → 𝓅𝓅 X
%% Δ .. (y : U)    = (p : U → *) ≡> σ y p → p (τσ y)
%% wf              = (p : U → *) ≡> ((y : U) ≡> σ y p → p y) → p Ω


%%% hurkens.typer ends here.
