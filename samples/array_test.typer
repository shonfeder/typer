%%%%
%%%% Unit test for builtin primitive `Array`
%%%%

List->Array = list.List->Array;

len = Array_length;

get = Array_get (-1);

a0 : Array Int;
a0 = Array_create 0 0;

a1 : Array Int;
a1 = Array_create 5 0;

a2 : Array Int;
a2 = Array_append 2 (Array_append 1 a0);

a3 : Array Int;
a3 = Array_append 6 (Array_append 5 a1);

a4 : Array Int;
a4 = Array_set 1 1 (Array_set 2 2 (Array_set 3 3 (Array_set 4 4 a3)));

empty : Array Int;
empty = Array_empty ();

empty2 : Array Int;
empty2 = List->Array nil;

from-list : Array Int;
from-list = List->Array (cons 0 (cons 1 (cons 2 nil)));

test-length = do {
  Test_info "ARRAY" "length";

  r0 <- Test_eq "a0" (len a0) 0;
  r1 <- Test_eq "empty" (len empty) (len a0);
  r2 <- Test_eq "a1" (len a1) 5;
  r3 <- Test_eq "a2" (len a2) 2;
  r4 <- Test_eq "a3" (len a3) 7;
  r5 <- Test_eq "a4" (len a4) 7;

  success <- IO_return (and r0 (and r1 (and r2 (and r3 (and r4 r5)))));

  if success then
    (Test_info "ARRAY" "test on length succeeded")
  else
    (Test_warning "ARRAY" "test on length failed");

  IO_return success;
};

test-get = do {
  Test_info "ARRAY" "get";

  r0 <- Test_eq "a1" (get 3 a1) 0;
  r1 <- Test_eq "a2" (get 0 a2) 1;
  r2 <- Test_eq "a3" (get 6 a3) 6;
  r3 <- Test_eq "a4" (get 3 a4) 3;
  r4 <- Test_eq "out of bounds" (get 10 a4) (-1);

  success <- IO_return (and r0 (and r1 (and r2 (and r3 r4))));

  if success then
    (Test_info "ARRAY" "test on get succeeded")
  else
    (Test_warning "ARRAY" "test on get failed");

  IO_return success;
};

test-from-list = do {
  Test_info "ARRAY" "List->Array";

  r0 <- Test_eq "empty2" (len empty2) 0;
  r1 <- Test_eq "len" (len from-list) 3;
  r2 <- Test_eq "from-list 0" (get 0 from-list) 0;
  r3 <- Test_eq "from-list 1" (get 1 from-list) 1;
  r4 <- Test_eq "from-list 2" (get 2 from-list) 2;
  r5 <- Test_eq "out of bounds" (get 3 from-list) (-1);

  success <- IO_return (and r0 (and r1 (and r2 (and r3 (and r4 r5)))));

  if success then
    (Test_info "ARRAY" "test on List->Array succeeded")
  else
    (Test_warning "ARRAY" "test on List->Array failed");

  IO_return success;
};

exec-test = do {
  b1 <- test-length;
  b2 <- test-get;
  b3 <- test-from-list;

  IO_return (and b1 (and b2 b3));
};
